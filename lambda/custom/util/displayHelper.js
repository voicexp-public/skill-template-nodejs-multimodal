/*jslint node: true */
/*jshint esversion: 6 */

'use strict';

const Alexa = require('alexa-sdk');
const makePlainText = Alexa.utils.TextUtils.makePlainText;
const makeRichText = Alexa.utils.TextUtils.makeRichText;
const makeImage = Alexa.utils.ImageUtils.makeImage;

const _ = require('lodash');
const config = require('../config.json');


function renderWelcomeScreen(params) {

    const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

    const template = builder
        .setBackgroundImage(makeImage(`https://s3.amazonaws.com/${config.s3BucketName}/images/background-welcome.png`))
        .build();

    this.response.renderTemplate(template) // jshint ignore:line
        .hint(params.hint);
}

function renderGoodbyeScreen(params) {

    const builder = new Alexa.templateBuilders.BodyTemplate6Builder();

    const template = builder
        .setBackgroundImage(makeImage(`https://s3.amazonaws.com/${config.s3BucketName}/images/background-goodbye.png`))
        .build();

    this.response.renderTemplate(template); // jshint ignore:line
}


function supportsDisplay() {

    /* jshint ignore:start */
    let hasDisplay =
        this.event &&
        this.event.context &&
        this.event.context.System &&
        this.event.context.System.device &&
        this.event.context.System.device.supportedInterfaces &&
        this.event.context.System.device.supportedInterfaces.Display;

    return hasDisplay;
    /* jshint ignore:end */

}

module.exports.supportsDisplay = supportsDisplay;
module.exports.renderWelcomeScreen = renderWelcomeScreen;
module.exports.renderGoodbyeScreen = renderGoodbyeScreen;