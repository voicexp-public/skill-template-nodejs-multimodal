/*jslint node: true */
/*jshint esversion: 6 */
"use strict";

console.log('Handler: defaultHandlers');

// const _ = require('lodash');
// const config = require('../config.json');
const TranslationHelper = require('../util/translationHelper');
const sessionHelper = require('../util/sessionHelper');
const displayHelper = require('../util/displayHelper');

const handlers = {

    'NewSession': function () {
        console.log('defaultHandlers ---> NewSession');

        let isFirstTimeSkillLaunched = false;
        this.attributes.sessionTemp = {};

        if (!this.attributes.skillInfo) {
            isFirstTimeSkillLaunched = true;

            // init skill default values
            this.attributes.skillInfo = {
                launchCount: 0,
                oneShotCount: 0,
                firstAccessed: new Date().toUTCString()
            };
        }

        let eventString = 'Unhandled';

        if (this.event.request.type === 'LaunchRequest') {
            this.attributes.sessionTemp.isConversationMode = true;
            this.attributes.skillInfo.launchCount += 1;

            if (isFirstTimeSkillLaunched === true) {
                eventString = 'PrivateWelcomeFirst';
            }
            else {
                eventString = 'PrivateWelcomeBack';
            }

        } else if (this.event.request.type === 'IntentRequest') {

            this.attributes.sessionTemp.isConversationMode = false;
            this.attributes.skillInfo.oneShotCount += 1;

            eventString = this.event.request.intent.name;
        }

        this.attributes.skillInfo.lastAccessed = new Date().toUTCString();

        this.emit(eventString);
    },

    'PrivateWelcomeFirst': function () {
        console.log('defaultHandlers ---> PrivateWelcomeFirst');

        let translations = new TranslationHelper(this).getTranslationsWithFallback('PrivateWelcomeFirst');

        // always conversation mode
        this.response.speak(translations.speechWithPrompt)
            .listen(translations.reprompt);

        // render display
        if (displayHelper.supportsDisplay.call(this)) {
            displayHelper.renderWelcomeScreen.call(this, { hint: translations.displayHint });
        }

        this.emit(':responseReady');
    },

    'PrivateWelcomeBack': function () {
        console.log('defaultHandlers ---> PrivateWelcomeBack');

        this.emit('PrivateHandleIntent', 'PrivateWelcomeBack', null, displayHelper.renderWelcomeScreen, null);

        // let translations = new TranslationHelper(this).getTranslationsWithFallback('PrivateWelcomeBack');

        // // always conversation mode
        // this.response.speak(translations.speechWithPrompt)
        //     .listen(translations.reprompt);

        // // render display
        // if (displayHelper.supportsDisplay.call(this)) {
        //     displayHelper.renderWelcomeScreen.call(this, { hint: translations.displayHint });
        // }

        // this.emit(':responseReady');
    },

    'AMAZON.HelpIntent': function () {
        console.log('defaultHandlers ---> AMAZON.HelpIntent');

        let translations = new TranslationHelper(this).getTranslationsWithFallback('AMAZONHelpIntent');

        // always conversation mode
        this.response.speak(translations.speechWithPrompt)
            .listen(translations.reprompt);

        // render display

        this.emit(':responseReady');
    },

    'AMAZON.RepeatIntent': function () {
        console.log('defaultHandlers ---> AMAZON.RepeatIntent');

        let speech = this.attributes.sessionTemp.speech;
        let prompt = this.attributes.sessionTemp.prompt;
        let speechWithPrompt = speech + '<break time="300ms"/>' + prompt;
        let reprompt = this.attributes.sessionTemp.reprompt;

        this.emit(':ask', speechWithPrompt, reprompt);
    },

    'PrivateShortRepeat': function () {
        console.log('defaultHandlers ---> PrivateShortRepeat');

        let prompt = this.attributes.sessionTemp.prompt;
        let reprompt = this.attributes.sessionTemp.reprompt;

        this.emit(':ask', prompt, reprompt);
    },

    'PrivateHandleIntent': function (translationKey, translationData, displayFunction, displayData) {
        console.log('defaultHandlers ---> PrivateHandleIntent');

        if (!translationKey) {
            translationKey = 'PrivateMissingTranslationKey';
        }

        if (!translationData) {
            translationData = {};
        }

        if (!displayData) {
            displayData = {};
        }

        // get translations

        let translations = new TranslationHelper(this).getTranslationsWithFallback(translationKey, translationData);

        if (this.attributes.sessionTemp.isConversationMode) {
            this.response.speak(translations.speechWithPrompt)
                .listen(translations.reprompt);
        }
        else {
            this.response.speak(translations.speech);
        }



        // render display

        if (displayFunction && displayHelper.supportsDisplay.call(this)) {

            let defaultDisplayData = {
                title: translations.displayTitle,
                hint: translations.displayHint
            };

            let mergedDisplayData = Object.assign({}, defaultDisplayData, displayData);

            displayFunction.call(this, mergedDisplayData);
        }

        this.emit(':responseReady');
    },

    'AMAZON.CancelIntent': function () {
        console.log('defaultHandlers ---> AMAZON.CancelIntent');

        this.emit('SessionEndedRequest');
    },

    'AMAZON.StopIntent': function () {
        console.log('defaultHandlers ---> AMAZON.StopIntent');

        this.emit('SessionEndedRequest');
    },


    'SessionEndedRequest': function () {
        console.log('defaultHandlers ---> SessionEndedRequest');

        let translations = new TranslationHelper(this).getTranslationsWithFallback('SessionEndedRequest');
        this.response.speak(translations.speech);

        // place at the end of every one-shot intent and SessionEndedRequest
        sessionHelper.beforeEndSession.call(this);

        // render display
        if (displayHelper.supportsDisplay.call(this)) {
            displayHelper.renderGoodbyeScreen.call(this, {});
        }

        this.emit(':responseReady');
    },

    'Unhandled': function () {
        console.log('defaultHandlers ---> Unhandled');

        let translations = new TranslationHelper(this).getTranslationsWithFallback('Unhandled');

        // always conversation mode
        this.emit(':ask', translations.speechWithPrompt, translations.reprompt);
    },

};

module.exports = handlers;