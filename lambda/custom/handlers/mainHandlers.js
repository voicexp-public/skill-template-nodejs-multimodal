/*jslint node: true */
/*jshint esversion: 6 */
"use strict";

console.log('Handler: mainHandlers');

// const _ = require('lodash');
const config = require('../config.json');
const TranslationHelper = require('../util/translationHelper');
const displayHelper = require('../util/displayHelper');

const handlers = {

    'SampleNoDisplayIntent': function () {
        console.log('mainHandlers ---> SampleNoDisplayIntent');

        this.emit('PrivateHandleIntent', 'SampleNoDisplayIntent');
    },

};

module.exports = handlers;
