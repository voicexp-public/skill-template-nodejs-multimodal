/*jslint node: true */
/*jshint esversion: 6 */
'use strict';

const Alexa = require("alexa-sdk");
const AWS = require('aws-sdk');
const config = require('./config.json');
const translations = require('./translations.json');

exports.handler = function (event, context) {
    console.log(JSON.stringify(event, null, '  '));

    if (config.debugLocal == true) {

        // needed for running locally with Bespoken Tools
        // without explicitly setting the region, you will get the error:
        //     Unhandled Exception from Lambda: Error: Error fetching user state: ConfigError: Missing region in config        
        AWS.config.update({ region: config.region });
    }

    var alexa = Alexa.handler(event, context);

    alexa.appId = config.appId;
    alexa.resources = translations;

    if (config.dynamoDBTableName && config.dynamoDBTableName !== '') {
        alexa.dynamoDBTableName = config.dynamoDBTableName;
    }

    alexa.registerHandlers(
        require('./handlers/defaultHandlers'),
        require('./handlers/mainHandlers')
    );

    alexa.execute();
};